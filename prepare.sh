#!/bin/bash

# load package list
source packages.sh

ORIG_IMAGE=bookworm-amd64.qcow2
OUT_IMAGE=deb12.qcow2

set -ex

if [[ ! -f "${ORIG_IMAGE}" ]]; then
	wget -q https://cloud.debian.org/images/cloud/bookworm/20240507-1740/debian-12-nocloud-amd64-20240507-1740.qcow2 -O "${ORIG_IMAGE}"
fi

rm -f "${OUT_IMAGE}"

cp $ORIG_IMAGE $OUT_IMAGE

# The image has only a little space
qemu-img create -f qcow2 -o preallocation=metadata "${OUT_IMAGE}" 3G
virt-resize --format qcow2 --expand /dev/sda1 "${ORIG_IMAGE}" "${OUT_IMAGE}"

virt-customize \
	-a "${OUT_IMAGE}" \
	--update \
	--uninstall chrony,unattended-upgrades,apparmor,ifupdown,iptables,logrotate,rsyslog,whiptail \
	--install "${PACKAGE_LIST_COMMAS}" \
	--run-command "useradd -m -s /bin/bash progtest" \
	--root-password password:root \
	--password progtest:password:progtest \
	--timezone "Europe/Prague" \
	--hostname "pa2" \
	--run-command 'grub-install /dev/sda'

# debian's original rootfs was at /dev/sda1 at the beginning of the image
# but the copying and resizing moved it at the end as /dev/sda3 so we need to reinstall grub
# It turns out that we can do that from virt-customize, but virt-rescue is also way, yet
# unfortunately it can't be scripted

# virt-rescue -i --format=qcow2 -a "${OUT_IMAGE}"
	# chroot /sysroot
	# grub-install /dev/sda
	# exit
	# exit
