PACKAGES=(acl
	aptitude
	bc
	bzip2
	doxygen
	gcc
	g++
	gdb
	hexedit
	joe
	less
	lsof
	lzma
	lzop
	make
	mc
	openssl
	openssh-server
	openssh-client
	patch
	pmccabe
	psmisc
	rsync
	screen
	splint
	sqlite3
	strace
	unzip
	valgrind
	vim
	wget
	zip
	zstd
	libncurses-dev
	libsdl2-dev
	libsdl2-gfx-dev
	libsdl2-image-dev
	libsdl2-mixer-dev
	libsdl2-net-dev
	libsdl2-ttf-dev
	libgl-dev
	libglu-dev
	freeglut3-dev
	libssl-dev
	zlib1g-dev
	# libmariadbclient-dev # NOT IN DEBIAN 11!
	# > Package libmariadbclient-dev is not available, but is referred to by another package.
	# > However the following packages replace it: libmariadb-dev-compat libmariadb-dev
	libmariadb-dev # probably?
	libpq-dev
	libsqlite3-dev
	libxml2-dev
	libpng-dev
	libjpeg-dev
	libexif-dev
	libexiv2-dev
	libmediainfo-dev
)

printf -v PACKAGE_LIST_SPACES '%s ' "${PACKAGES[@]}"

printf -v PACKAGE_LIST_COMMAS '%s,' "${PACKAGES[@]}"
PACKAGE_LIST_COMMAS="${PACKAGE_LIST_COMMAS%,}"

